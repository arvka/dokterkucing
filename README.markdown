# Dokterkucing

[College Project]

Aplikasi sederhana untuk mendeteksi penyakit Kucing

## Quickstart

- `(ql:quickload "dokterkucing")`
- `(dokterkucing:start :port 8000)`
- `(dokterkucing:stop)`

## Credits

Library or Application used by this project

- Hunchentoot Web Server
- Caveman2 Web Framework
- Djula HTML Templating System
- Twitter Bootrstrap
- Awesome Icons

## Author

* RP Tim X

## Copyright

Copyright (c) 2017 RP Tim X

